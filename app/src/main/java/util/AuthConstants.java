package util;

public class AuthConstants {
    public static final String APP_KEY = "appKey";
    public static final String TIME_STAMP = "timeStamp";
    public static final String APP_ID = "appId";
    public static final String TOKEN = "token";
}
