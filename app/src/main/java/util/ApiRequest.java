package util;

import java.util.Map;
public class ApiRequest {
    /**
     * 加密后的token
     */
    private String token;

    /**
     * 申请唯一标识
     */
    private Long appId;

    /**
     * 调用方身份标识
     */
    private String appKey;

    /**
     * 调用方调用时的时间戳
     */
    private long timeStamp;

    public ApiRequest(String token, Long appId, String appKey, long timeStamp) {
        this.token = token;
        this.appId = appId;
        this.appKey = appKey;
        this.timeStamp = timeStamp;
    }

    public static ApiRequest buildApiRequest(Map<String, String> paramsMap) {
        if (paramsMap == null || paramsMap.size() < 1) {
            return null;
        }
        String token = paramsMap.containsKey(AuthConstants.TOKEN) ? paramsMap.get(AuthConstants.TOKEN) : null;
        String appKey = paramsMap.containsKey(AuthConstants.APP_KEY) ? paramsMap.get(AuthConstants.APP_KEY) : null;
        String appId = paramsMap.containsKey(AuthConstants.APP_ID) ? paramsMap.get(AuthConstants.APP_ID) : null;
        String timeStamp = paramsMap.containsKey(AuthConstants.TIME_STAMP) ? paramsMap.get(AuthConstants.TIME_STAMP) : null;
        return new ApiRequest(token, Long.valueOf(appId), appKey, Long.valueOf(timeStamp));
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Long getAppId() {
        return appId;
    }

    public void setAppId(Long appId) {
        this.appId = appId;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "ApiRequest{" +
                ", token='" + token + '\'' +
                ", appId=" + appId +
                ", appKey='" + appKey + '\'' +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
