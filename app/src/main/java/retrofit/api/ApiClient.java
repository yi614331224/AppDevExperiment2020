package retrofit.api;

import android.content.Context;

import java.util.Map;

import retrofit.bean.EditResultBean;
import retrofit.bean.EditSendBean;
import retrofit.bean.LoginResultBean;
import retrofit.bean.LoginSendBean;
import retrofit.bean.RegisterResultBean;
import retrofit.bean.RegisterSendBean;
import retrofit.bean.ResetPwdResultBean;
import retrofit.bean.ResetPwdSendBean;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;

public class ApiClient {
    private Retrofit mRetrofit;
    private Api mApi;
    private static ApiClient sApiClient;
    private ApiClient(final Context context){
        mRetrofit = new Retrofit.Builder().baseUrl(GetApi.baseURL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        mApi = mRetrofit.create(Api.class);
    }
    public static ApiClient getInstance(Context context){
        if(sApiClient == null){
            sApiClient = new ApiClient(context);
        }
        return  sApiClient;
    }
    public static void destoryInstance() {
        if(sApiClient != null){
            sApiClient = null;
        }
    }
    public String getRootURL(){
        return GetApi.baseURL;
    }
    public Call<RegisterResultBean> getReturnData(Map<String,String> map, @Body RegisterSendBean registerSendBean) {
        return mApi.register(map, registerSendBean);
    }

    public Call<LoginResultBean> getReturnData(Map<String,String> map, @Body LoginSendBean loginSendBean){
        return mApi.login(map,loginSendBean);
    }

    public Call<ResetPwdResultBean> getReturnData(Map<String,String> map, @Body ResetPwdSendBean resetPwdSendBean){
        return mApi.resetPwd(map,resetPwdSendBean);
    }

    public Call<EditResultBean> getReturnData(Map<String,String> map, @Body EditSendBean editSendBean){
        return mApi.edit(map,editSendBean);
    }
}
