package retrofit.api;

import java.util.Map;

import retrofit.bean.EditResultBean;
import retrofit.bean.EditSendBean;
import retrofit.bean.LoginResultBean;
import retrofit.bean.LoginSendBean;
import retrofit.bean.RegisterResultBean;
import retrofit.bean.RegisterSendBean;
import retrofit.bean.ResetPwdResultBean;
import retrofit.bean.ResetPwdSendBean;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface Api {
    @POST(GetApi.register)
    Call<RegisterResultBean> register(@QueryMap Map<String,String>map, @Body RegisterSendBean registerSendBean);
    @POST(GetApi.login)
    Call<LoginResultBean> login(@QueryMap Map<String, String> map, @Body LoginSendBean loginSendBean);

    @POST(GetApi.resetPwd)
    Call<ResetPwdResultBean> resetPwd(@QueryMap Map<String, String> map, @Body ResetPwdSendBean resetPwdSendBean);

    @POST(GetApi.edit)
    Call<EditResultBean> edit(@QueryMap Map<String, String> map, @Body EditSendBean editSendBean);
}
