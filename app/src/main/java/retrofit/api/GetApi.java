package retrofit.api;

public class GetApi {
    public final static String baseURL = "http://43.138.21.29:8010/";
    public final static String register = "app/user/api/register";
    public final static String login = "app/user/api/login";
    public final static String resetPwd = "app/user/api/resetPwd";
    public final static String edit = "app/user/api/edit";
}
