package retrofit.bean;

public class LoginResultBean {

    /**
     * msg : 操作成功
     * classesId : 110
     * code : 200
     * userName : 32209990
     * nickName : 测试
     * userId : 3
     */

    private String msg;
    private int classesId;
    private int code;
    private String userName;
    private String nickName;
    private int userId;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getClassesId() {
        return classesId;
    }

    public void setClassesId(int classesId) {
        this.classesId = classesId;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
