package retrofit.bean;

public class ResetPwdSendBean {
    /**
     * userId : 5
     * password : 123456
     * updateBy : 内大考研
     */

    private String userId;
    private String password;
    private String updateBy;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }
}
