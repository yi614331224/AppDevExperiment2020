package cn.imu.appdevexperiment2020;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MyActivity extends BaseActivity {

    private static final String TAG = "MyActivity";

    public static void start(Context context, String userName) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra("userName : ", userName);
        context.startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
    }


}

/*
    public void intentClick(View view) {
        //显式跳转
        //Intent intent = new Intent(this, MainActivity.class);
        //隐式跳转
        //Intent intent = new Intent("cn.imu.appdevexperiment2020.MainActivity");

        //传递数据
        //intent.putExtra("intentData", "要传递的数据");
        //startActivity(intent);

        //返回数据
        //startActivityForResult(intent,111);

        MyActivity.start(this,"test");

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        String text = data.getStringExtra("text");

        Log.d(TAG,"requestCode = " + requestCode + ",resultCode = " + resultCode + ",data =  " + text);
    }
}*/