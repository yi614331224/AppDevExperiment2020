package cn.imu.appdevexperiment2020;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import retrofit.api.ApiClient;
import retrofit.bean.EditResultBean;
import retrofit.bean.EditSendBean;
import retrofit.bean.RegisterResultBean;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import util.AuthUtil;

public class Edit extends AppCompatActivity {

    EditText NickName;
    EditText UserName;
    EditText Phone ;
    EditText Email ;
    EditText Classid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        NickName = (EditText) findViewById(R.id.ed_nickname);
        UserName = (EditText) findViewById(R.id.ed_username);

        Phone = (EditText) findViewById(R.id.ed_phone);
        Email = (EditText) findViewById(R.id.ed_email);
        Classid = (EditText) findViewById(R.id.ed_classid);

    }

    public void EditIm(View view) {
        String username = UserName.getText().toString();
        String nickname = NickName.getText().toString();
        String phone = Phone.getText().toString();
        String email = Email.getText().toString();
        String classid = Classid.getText().toString();

        EditSendBean editSendBean = new EditSendBean();
        editSendBean.setUserName(username);
        editSendBean.setNickName(nickname);
        editSendBean.setPhonenumber(phone);
        editSendBean.setEmail(email);
        editSendBean.setClassesId(classid);
        editSendBean.setUserId("3");
        editSendBean.setUpdateBy("app");
        Call<EditResultBean> call = ApiClient.getInstance(getApplicationContext())
                .getReturnData(AuthUtil.GetAuth(),editSendBean);
        call.enqueue(new Callback<EditResultBean>() {
            @Override
            public void onResponse(Call<EditResultBean> call, Response<EditResultBean> response) {
                if(response.body() != null){
                    if(response.body().getCode() == 200){
                        ToastUtil.showToast(getApplicationContext(),"信息修改成功");
                        finish();
                    }else {
                        ToastUtil.showToast(getApplicationContext(),response.body().getMsg().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<EditResultBean> call, Throwable t) {
                ToastUtil.showToast(getApplicationContext(),"网络请求错误");
            }
        });
    }
}