package cn.imu.appdevexperiment2020;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.TextView;

public class HandlerT extends AppCompatActivity {

    private TextView tview;
    TextView textView;
    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);

            if(msg.what == 0){
                String strData = (String)msg.obj;
                textView = (TextView) findViewById(R.id.handlertext);
                textView.setText(strData);
                ToastUtil.showToast(getApplicationContext(),"已接受到了子线程的消息");
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handler_t);
    }

    public void start(View view) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String string = "hello";
                Message message = new Message();
                message.what = 0;
                message.obj = string;
                try {
                    Thread.sleep(6 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mHandler.sendMessage(message);
            }
        }).start();
        ToastUtil.showToast(getApplicationContext(),"这里是主线程");
    }
}