package cn.imu.appdevexperiment2020;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import retrofit.api.ApiClient;
import retrofit.bean.RegisterResultBean;
import retrofit.bean.ResetPwdResultBean;
import retrofit.bean.ResetPwdSendBean;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import util.AuthUtil;

public class RePassword extends AppCompatActivity {

    EditText Password ;
    EditText RePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_re_password);
        Password = (EditText) findViewById(R.id.rp_password);
        RePassword = (EditText) findViewById(R.id.rp_repassword);
    }

    public void toResetPwd(View view) {
        String password = Password.getText().toString();
        String repassword = RePassword.getText().toString();
        ResetPwdSendBean resetPwdSendBean= new ResetPwdSendBean();
        resetPwdSendBean.setUserId("5");
        resetPwdSendBean.setPassword(password);
        resetPwdSendBean.setUpdateBy("app");

        Call<ResetPwdResultBean> call = ApiClient.getInstance(getApplicationContext())
                .getReturnData(AuthUtil.GetAuth(),resetPwdSendBean);
        call.enqueue(new Callback<ResetPwdResultBean>() {
            @Override
            public void onResponse(Call<ResetPwdResultBean> call, Response<ResetPwdResultBean> response) {
                if(response.body() != null){
                    if(response.body().getCode() == 200){
                        ToastUtil.showToast(getApplicationContext(),"密码修改成功");
                        finish();
                    }else {
                        ToastUtil.showToast(getApplicationContext(),response.body().getMsg().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResetPwdResultBean> call, Throwable t) {
                ToastUtil.showToast(getApplicationContext(),"网络请求错误");
            }
        });
    }
}