package cn.imu.appdevexperiment2020;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

public class AddFragmentActivity extends AppCompatActivity {

    private HomeFragment homeFragment;
    private FragmentManager supportFragmentManager;
    private FragmentTransaction fragmentTransaction;
    private PeoFragment peoFragment;
    private RadioGroup mRg;
    private List<Fragment> mFragments = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_fragment);

        mRg = findViewById(R.id.rg_main);

        initView();
    }

    private void initView() {
        //1、拿到FragmentManager
        supportFragmentManager = getSupportFragmentManager();
        //2、拿到fragment的fragmentTransaction
        fragmentTransaction = supportFragmentManager.beginTransaction();

        //默认  联系人为主页
        mRg.check(R.id.rb_people);
        //3、实例化Fragment
        peoFragment = new PeoFragment();
        mFragments.add(peoFragment);
        hideOthersFragment(peoFragment, true);
        mRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i){
                    case R.id.rb_people:
                        hideOthersFragment(peoFragment, false);
                        break;
                    case R.id.rb_message:
                        if (homeFragment == null) {
                            homeFragment = new HomeFragment();
                            mFragments.add(homeFragment);
                            hideOthersFragment(homeFragment, true);
                        } else {
                            hideOthersFragment(homeFragment, false);
                        }
                        break;
                }
            }
        });
    }

    private void hideOthersFragment(Fragment showFragment, boolean add) {
        fragmentTransaction = supportFragmentManager.beginTransaction();
        if (add) {
            //4、添加fragment
            fragmentTransaction.add(R.id.frameLayout, showFragment);
        }

        for (Fragment fragment : mFragments) {
            if (showFragment.equals(fragment)) {
                fragmentTransaction.show(fragment);
            } else {
                fragmentTransaction.hide(fragment);
            }
        }
        //5、调用commit使fragment生效
        fragmentTransaction.commit();
    }

    public void toChat(View view) {
        startActivity(new Intent(this, ChatActivity.class));
    }
}