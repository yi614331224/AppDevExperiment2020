package cn.imu.appdevexperiment2020;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import retrofit.api.ApiClient;
import retrofit.bean.LoginResultBean;
import retrofit.bean.LoginSendBean;
import retrofit.bean.RegisterResultBean;
import retrofit.bean.RegisterSendBean;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import util.AuthUtil;

public class MainActivity extends BaseActivity {

    private static final String TAG = "MainActivity";

    EditText UserName;
    EditText Password ;
    int flag = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //传递数据
       // Intent intent = getIntent();
       // String intentData = intent.getStringExtra("intentData");
      //  Log.d(TAG, "onCreat" + intentData);

        //返回数据
        Intent intent = new Intent();
        intent.putExtra("text", "text");
        setResult(1,intent);

        UserName = (EditText) findViewById(R.id.et_username);
        Password = (EditText) findViewById(R.id.et_password);

        //拿到EditText文本内容
        EditText etContent = this.findViewById(R.id.et_username);
        etContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d(TAG,"beforeTextChanged -> " + charSequence.toString());
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Log.d(TAG,"onTextChanged -> " + charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
                Log.d(TAG,"afterTextChanged -> " + editable.toString());
            }
        });
    }

    public void intentMyActivity(View view) {

        String username = UserName.getText().toString();
        String password = Password.getText().toString();
        LoginSendBean loginSendBean = new LoginSendBean();

        loginSendBean.setUserName(username);
        loginSendBean.setPassword(password);

        Call<LoginResultBean> call = ApiClient.getInstance(getApplicationContext()).getReturnData(AuthUtil.GetAuth(),loginSendBean);
        call.enqueue(new Callback<LoginResultBean>() {
            @Override
            public void onResponse(Call<LoginResultBean> call, Response<LoginResultBean> response) {
                if(response.body() != null){
                    if(response.body().getCode() == 200){
                        Log.e(TAG,response.body().getMsg());
                        ToastUtil.showToast(getApplicationContext(),"登录成功");
                        flag=  1;
                    }else{
                        ToastUtil.showToast(getApplicationContext(),response.body().getMsg().toString());
                    }
                }else{
                    ToastUtil.showToast(getApplicationContext(),"网络请求错误");
                }
            }
            @Override
            public void onFailure(Call<LoginResultBean> call, Throwable t) {
                ToastUtil.showToast(getApplicationContext(),"网络请求错误");
            }
        });
        if(flag == 1)
            startActivity(new Intent(this, AddFragmentActivity.class));
    }

    public void toRegister(View view) {
        startActivity(new Intent(this, Register.class));
    }

    public void toRePassword(View view) {
        startActivity(new Intent(this, RePassword.class));
    }

    public void toEdit(View view) {
        startActivity(new Intent(this, Edit.class));
    }
}