package cn.imu.appdevexperiment2020;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import retrofit.api.ApiClient;
import retrofit.bean.RegisterResultBean;
import retrofit.bean.RegisterSendBean;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import util.AuthUtil;

public class Register extends AppCompatActivity {

    private static final String TAG = "Register";
  /*  @BindView(R.id.rq_nickname)
    TextInputLayout rqNickname;
    @BindView(R.id.rq_username)
    TextInputLayout rqUsername;
    @BindView(R.id.rq_password)
    TextInputLayout rqPassword;
    @BindView(R.id.rq_repassword)
    TextInputLayout rqrePassword;
    @BindView(R.id.rq_phone)
    TextInputLayout rqPhone;
    @BindView(R.id.rq_email)
    TextInputLayout rqEmail;
    @BindView(R.id.rq_classid)
    TextInputLayout rqClassid;*/

    EditText NickName;
    EditText UserName;
    EditText Password ;
    EditText Repassword;
    EditText Phone ;
    EditText Email ;
    EditText Classid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
         NickName = (EditText) findViewById(R.id.rq_nickname);
         UserName = (EditText) findViewById(R.id.rq_username);
         Password = (EditText) findViewById(R.id.rq_password);
         Repassword = (EditText) findViewById(R.id.rq_repassword);
         Phone = (EditText) findViewById(R.id.rq_phone);
         Email = (EditText) findViewById(R.id.rq_email);
         Classid = (EditText) findViewById(R.id.rq_classid);
        Log.e(TAG,UserName.getText().toString());
    }



    public void Register(View view) {
        String username = UserName.getText().toString();
        String nickname = NickName.getText().toString();
        String password = Password.getText().toString();
        String repassword = Repassword.getText().toString();
        String phone = Phone.getText().toString();
        String email = Email.getText().toString();
        String classid = Classid.getText().toString();

        RegisterSendBean registerSendBean = new RegisterSendBean();
        registerSendBean.setUserName(username);
        registerSendBean.setNickName(nickname);
        registerSendBean.setPassword(password);
        registerSendBean.setPhonenumber(phone);
        registerSendBean.setClassesId(classid);


        Call<RegisterResultBean> call = ApiClient.getInstance(getApplicationContext())
                        .getReturnData(AuthUtil.GetAuth(),registerSendBean);
        call.enqueue(new Callback<RegisterResultBean>() {
            @Override
            public void onResponse(Call<RegisterResultBean> call, Response<RegisterResultBean> response) {
                if(response.body() != null){
                    if(200 == response.body().getCode()){
                        Log.e(TAG,response.body().getMsg());
                        ToastUtil.showToast(getApplicationContext(),"注册成功");
                        finish();
                    }else{
                        ToastUtil.showToast(getApplicationContext(),response.body().getMsg().toString());
                    }
                }else{
                    /*ToastUtil.showToast(getApplicationContext(),"网络请求错误");*/
                }
            }

            @Override
            public void onFailure(Call<RegisterResultBean> call, Throwable t) {
                ToastUtil.showToast(getApplicationContext(),"网络请求错误");
            }
        });
    }
}